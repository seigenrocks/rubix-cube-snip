# Rubix cube snip

Definition of a snip that draws a Rubix cube with optional highlights. Since it's a snip, opening cube.rkt in DrRacket and typing

```> (define c (new cube-snip%))```

```> (send c up)```

```> (send c right-)```

```> (send c up2)```

```> (send c cubex)```

shows the cube with these operations applied to it. I should make a GUI for this.
